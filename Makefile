##
## Makefile for tekadventure in /home/XU_P/Graphical/tekadventure
## 
## Made by Peter Xu
## Login   <XU_P@epitech.net>
## 
## Started on  Wed May 24 13:05:37 2017 Peter Xu
## Last update Thu Jun  1 12:29:02 2017 Bender_Jr
##

_LIB	=	base2.c 		\
		base.c			\
		f_fction.c 		\
		flag_match.c		\
		flgs.c			\
		fprintf.c 		\
		get_next_line.c		\
		printf.c 		\
		putnbr.c		\
		strchr.c		\
		stringbis.c		\
		string.c		\
		strtowordtab.c

_GCONF	=	get_conf.c		\
		fill_struct.c		\
		fill_pathlist.c		\
		utils_fiil.c

_SOUND	=	free_bufferz.c		\
		music.c 		\
		music_ctrl.c		\
		music_utils.c		\
		sound.c

_EVENT	=	handler.c		\
		event_mov.c

_GAME	=	game_loop.c		\
		destructor.c

_SPRITE	=	conf_to_sprite.c 	\
		text_sprite.c		\
		draw_sprite.c

_SRC	=	main.c

SRC	=	$(addprefix lib/, $(_LIB))		\
		$(addprefix src/get_conf/, $(_GCONF))	\
		$(addprefix src/sound/, $(_SOUND)) 	\
		$(addprefix src/events/, $(_EVENT)) 	\
		$(addprefix src/sprite/, $(_SPRITE)) 	\
		$(addprefix src/game/, $(_GAME))	\
		$(addprefix src/, $(_SRC))

OBJ	=	$(SRC:.c=.o)

CC	=	gcc

RM	=	rm -f

CFLAGS	+=	-Wall -Wextra -pedantic

CFLAGS	+=	-I include

LDFLAGS	+=	-lc_graph_prog_full

NAME	=	tekadventure

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

all: $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

dbg:
	$(CC) $(CFLAGS) $(SRC) -D DEBUG -g2 -o $(NAME) $(LDFLAGS)

32:
	$(CC) $(CFLAGS) $(SRC) -m32 -o $(NAME) $(LDFLAGS)

re: fclean all

.PHONY: all clean fclean re
