> **Tekadventure**
> Hein ? get ready, (b)fun mode -on

* Known bug : see "bug_list" opt
* Doc : see doc/html header option , Related pages, data structures ..
* Version : 0.1
* [TekAdenture-TheGame](https://tekadventure-thegame.org)

## Presented by: ##

*   * K*, X*, B* *

# Usage #

* 1 install doxygen like ```apt-get install doxygen``` or ```pacman -S doxygen``` or whatever
* 2 run ```./get_doc.sh -g``` or ```./get_doc -h``` to see script options :)
* 3 open doc/html/index.html with your favorite browser
* 4 profit