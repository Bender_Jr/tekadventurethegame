#!/bin/bash
## get_doc.sh for Tekadventure in /home/bender/Repo/tekadventure
## 
## Made by Bender_Jr
## Login   <@epitech.eu>
## 
## Started on  Fri May 26 23:52:34 2017 Bender_Jr
## Last update Sun May 28 10:20:14 2017 Bender_Jr
##

help='-h'
if [ "$#" -ne 1 ] || [ "$@" = "$help" ]
then
    echo -e "\nNote:\n\t\tPour que ce pseudo script fonctionne assurez vous\n\
    	     \td'avoir installé \0033[1mDoxygen\033[0m avec votre 'apt-get' favori\n
	     \tOptions : \n\
	     \t-c\tto remove docfile\n\
	     \t-h\tto see this help \n\
	     \t-g\tto generate html and xml doc\n\
	     \t-o\tto open it in the default web browser\n"
    exit 0
fi

clean='-c'
if [ $@ == $clean ]
then
    echo -e "\n\tRemoved doc/ dir\n"
    rm -rf doc
    exit 0
fi

gen='-g'
if [ $@ == $gen ]
then
    if [ -f doc/ ]; then
	mkdir doc;
    fi
    if which doxygen > /dev/null
    then
	doxygen Doxyfile;
    else
	echo -e "Install doxygen first, see -h for help\n"
	exit 1
    fi
    exit 0
fi

open='-o'
if [ $@ == $open ]
then
    xdg-open doc/html/index.html &
    exit 0
fi
