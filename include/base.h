/*
** base.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:41:54 2017 Bender_Jr
** Last update Fri May 26 23:14:45 2017 Bender_Jr
*/

/**
** @file   base.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:41:48 2017
**
** @brief  All my lib fction (better working with something i can fix)
**         stdlib, unistd , stdio and string.h function recode
**
*/
#ifndef BASE_H_
# define BASE_H_

# include "strtowordtab.h"
# include "fprint.h"
# include "printf.h"

/**
** lib/core/string*.c
*/
size_t		len(const char *str);
size_t		tab_len(char **tab);
int		strn_cmp(const char *s1, const char *s2, size_t n);
char		*my_strcat(char *dest, const char *src);
char		*my_strdup(const char *src);
char		*removespace(char *str);
char		*my_strcatvs(char *dest, const char *src);
char		*my_strcpy(char *dest, const char *src);
char		*my_strncpy(char *dest, const char *src, size_t n);
char		*xstrchr(const char *s, int c);
void		*xmemset(void *s, int c, size_t n);
void		*my_memmove(void *dest, const void *src, size_t n);

/**
** stdlib adapted
*/
int		my_stringisnum(const char *str);
int		my_atoi(const char *str);
int		pprint(const char *str, int fd);
char		**freetab(char **ptr);
char		*clean_free(char *ptr);
char		*epurstr(char *str, int c);

#endif /* !BASE_H_ */
