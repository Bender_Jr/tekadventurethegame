/*
** conf.h for TEKADVENTURE in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Wed May 24 13:34:13 2017 Bender_Jr
** Last update Fri Jun  2 12:40:26 2017 Bender_Jr
*/
/**
** @file   conf.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:27:42 2017
**
** @brief  Config file header defining lots of fction ptr,
**	   struct etc etc; Also define some limit in array size
**
** @bug    See by yourself ;)
*/

#ifndef CONF_H_
# define CONF_H_

/**
** for sfimage typedef
*/
# include "SFML/Graphics.h"

/**
** config filepath
*/
# define CONF	"ressources/conf.rc"

/**
** error message when $DISPLAY not set
*/
#define ENV_ERR "Tekadventure ERROR:\n\n\
\t$DISPLAY not set\n\nexiting\n"

/**
** @defgroup group1 conf_syntax
**  ce groupe contient le vocabulaire du fichier de
**  configuration
**  @{
*/
/**
** @brief comment indicator char
*/
# define CMT_IND	'#'

/**
** @brief file pathname token
*/
# define NAME_IND	"<f_name>"

/**
** @brief size values token
*/
# define SIZE_IND	"<size>"

/**
** @brief position values token
*/
# define POS_IND	"<pos>"

/**
** @brief scale values token
*/
# define SCALE_IND	"<scale>"

/**
** @brief sound pathname token
*/
# define SOUND_IND	"<sound_dir>"

/**
 **  @biref font pathname token
 */
# define FONT_IND	"<font_dir>"

/**
** @brief end of token mark
*/
# define END_IND	"</>"

/**
** @}
*/
/**
** @struct s_conf
** @brief => data buffer to be sent to the
** graphical part of this prog, will be cast to
** a sFVector float or int later on.
** @var s_conf::m_path
** sound filepath data
** @var s_conf::f_path
** filepathname (to be check before buffered by fill_fpath()
** @var s_conf::size_img
** int array later cast to sfVector 2i
** @var s_conf::position
** float array -> sfVector 2/3f later on
** @var s_conf::scale
** float array -> sfVector 2/3f later on
*/
/**
** @typedef t_conf
** @brief mandatory typedef on s_conf struct
*/
typedef struct		s_conf {
  char			*m_path, *f_path, *font_path;
  int			size_img[3];
  float			position[3], scale[3];
}			t_conf;

/**
**  Max filepath and coord field size
*/
#define MAX_KWD (256)

/**
** @typedef parseur
** @brief   fill_xxx functions pointer (class)
**          see below for protoypes
*/
typedef int (*parseur)(char *bfr, t_conf *ptr);

/**
** @struct s_voc
** @brief => Parseur / lexer structure see fields for details.
** @var s_voc::ind
** stands for "indicator" ,
** char array containing the lexer symbols (see define's)
** @var s_voc::index
** int to address c field symbol matched on read buffer to
** the corresponding function pointer
** @var s_voc::fill_xxx
** function pointer see parseur typedef for details
*/
/**
** @typedef t_voc
** @brief mandatory typedef on s_voc struct
*/
typedef struct	s_voc {
  char	        ind[12];
  int		index;
  parseur	fill_xxx;
}		t_voc;

/**
** @brief voc tab array declaration
*/
extern t_voc	lex_tab[];


/**
** @brief gather full lines from gnl, lexe'd it, and fill
** corresponding fields in struct sprite_conf
** @return a filled sprite_conf struct pointer tab(test[i], test[i+1] etc)
** or NULL on error
*/
t_conf	*get_conf(const int c_filefd, int element_nbr);

/**
** @brief malloc and memset a new sprite conf struct
** @return malloced pointer on sprite_conf struct
*/
void		*new_elem();

/**
** @brief fill the t_conf struct
** with the good function pointer in voc tab
** @return the sprite_conf with corresponding
** fields filled
*/
void		*get_data(char *bfr, t_voc pos, t_conf *fill);

/**
** @brief Debug 'print data in struct' fction (also used for conf to sprite test)
** note : this fonctions is only visible when compiling with 'make dbg'
** @return void
*/
void		print_test(t_conf *fill, void *sprite);

/**
** @brief self explanantory
** @return void
*/
void		clean_buff(void *c1, void *c2);

/**
** @brief pseudo constructor for lexer usage
** @return the t_voc object filled with parms (symbol and funct ptr "parseur")
*/
void		*init_lex(t_voc *lex_tab);

/**
** @brief check for pathname validity with open
** and fill the m_path field in sprite conf struct
** @return -1 in case of error and 0 in sucess
*/
int		fill_mpath(char *bfr, t_conf *ptr);

/**
** @brief check for pathname validity with open
** and fill the f_path field in sprite conf struct
** @return -1 in case of error and 0 in sucess
*/
int		fill_fpath(char *bfr, t_conf *ptr);

/**
** @brief check for valid font dir in conf
**         and fill the right field
** @return -1 in case of error, 0 on success
*/
int		fill_font_path(char *bfr, t_conf *ptr);

/**
** @brief check for valid size data
** and fill the size_img int array in sprite conf struct
** @return -1 in case of error and 0 on success
*/
int		fill_size(char *bfr, t_conf *ptr);

/**
** @brief check for valid pos data
** and fill the position float array in sprite conf struct
** @return -1 in case of error and 0 on success
** @bug use of atoff fction (could failed)
*/
int		fill_pos(char *bfr, t_conf *ptr);

/**
** @brief check for valid scale data
** and fill the scale float array in sprite conf struct
** @return -1 in case of error and 0 on success
** @bug use of atoff fction (could failed)
*/
int		fill_scale(char *bfr, t_conf *ptr);

#endif /* !CONF_H_ */
