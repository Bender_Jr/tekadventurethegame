/*
** event.h for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 17:05:53 2017 Bender_Jr
** Last update Mon May 29 12:14:06 2017 Bender_Jr
*/

/**
** @file   event.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 17:06:48 2017
**
** @brief  contain all typdef and struct for event handler objects
*/
#ifndef EVENT_H_
# define EVENT_H_

# include <SFML/Window.h>

/**
** @typedef music_event
** This fction pointter only control
** function related to music objects
*/
typedef int	(*music_event)(void *ptr);

/**
** @struct s_sound_event
** @brief  store a sfKey to match with
** the appropriate sound funct
** @var s_sound_event::key
** the matching key (like sfKeyA or B)
** @var s_sound_event::act
** the function pointer see "music_event typedef" for details
*/
/**
** @typedef t_sound_event
** @brief mandatory typedef on s_sound_event struct
*/
typedef struct	s_sound_event {
  sfKeyCode	key;
  music_event	act;
}		t_sound_event;

/**
** @typedef move_event
** @brief as its name suggest, handle sprite (character) movement events
*/
typedef int	(*move_event)(void *ptr);

/**
** @struct s_move_event
** @brief  store a sfKey to match with
** the appropriate move funct
** @var s_move_event::key
** the matching key (like sfKeyUp)
** @var s_sound_event::act
** the function pointer see "move_event typedef" for details
*/
/**
** @typedef t_move_event
** @brief mandatory typedef on s_move_event struct
*/
typedef struct	s_move_event {
  sfKeyCode	key;
  move_event	act;
}		t_move_event;

/**
** @struct s_event
** @brief  store an array of the 2 event
** options sound and move
** @var s_event::hdlr
** see t_sound_event def
** @var s_event::moov
** see t_move_event def
*/
/**
** @typedef t_event
** @brief mandatory typedef on s_event struct
*/
typedef struct	s_event {
  t_sound_event	hdlr[5];
  t_move_event	moov[6];
}		t_event;

/**
** @brief take returned event from sfWindows pollevent thread
**        and execute the corresponding funct
** @return void
*/
void		event_gestion(sfEvent evt, sfRenderWindow *window, void *ptr);

/**
** @brief move up sprite
*/
int		move_up(void *ptr);

/**
** @brief move down sprite
*/
int		move_down(void *ptr);

/**
** @brief move right sprite
*/
int		move_right(void *ptr);

/**
** @brief move left sprite
*/
int		move_left(void *ptr);

#endif /* !EVENT_H_ */
