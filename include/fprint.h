/*
** fprint.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:47:19 2017 Bender_Jr
** Last update Fri May 26 23:15:07 2017 Bender_Jr
*/

/**
** @file   fprint.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:45:40 2017
**
** @brief  fprintf stdio function recode (a bit uselless in this project)
*/
#ifndef FPRINT_H_
# define FPRINT_H_
/*
** for varag and FILE *stream def
*/
# include <stdio.h>
# include <stdarg.h>

/**
** @typedef fpr
**           xfprintf %arg > f_ctions
*/
typedef int	(*fpr)(FILE *stream, va_list list);

/**
** lib/fprintf.c
*/
void	init_ptr(fpr *tab);
int	flag_run(char c, fpr *tab, FILE *stream, va_list list);
int	xfprintf(FILE *stream, const char *format, ...);

/**
** lib/f_fction.c
*/
int	f_putnbr(long nb, FILE *stream);
int	f_str(FILE *stream, va_list list);
int	f_long(FILE *stream, va_list list);

#endif /* !FPRINT_H_ */
