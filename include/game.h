/*
** game.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:24:25 2017 Bender_Jr
** Last update Fri Jun  2 11:40:50 2017 Bender_Jr
*/

/**
** @file   game.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 14:28:35 2017
**
** @brief Begining of the game design elements
*/
#ifndef GAME_H_
# define GAME_H_

# include "conf.h"
# include "sound.h"
# include "event.h"
# include "sprite.h"

/**
** name of the game windows
*/
# define NAME		"TekAdventure"
/**
** 1080 value
*/
# define WINDOW_HEIGHT  300
/**
** 1920 value
*/
# define WINDOW_WIDTH   300

/**
** @brief s_game struct contain all
** other structure used in game execution
** @var s_game::music
** @brief a pointer on music soundbank
** @var s_game::sprite
** @brief a pointer on sprite elements
*/
typedef struct	s_game	{
  t_music	*music;
  t_sprite	*sprite;
  sfText	*text[2];
  t_conf	*conf;
}		t_game;

/**
** @brief storage name index for
** sprite to draw and manipulate
*/
typedef enum	e_scene  {
  MAP = 0,
  CMAP = 1,
  P1 = 2,
  G1 = 3
}		t_scene;

/**
** @brief a temporary game loop
** @arg a pointer on t_game struct
** @return 0
*/
int		game_loop(t_game *ptr);

/**
** @brief clean ressources used in game (free etc)
**
*/
void		*clean_ressources(t_game *ptr);
#endif /* !GAME_H_ */
