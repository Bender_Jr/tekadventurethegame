/*
** get_next_line.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:44:29 2017 Bender_Jr
** Last update Fri May 26 23:42:48 2017 Bender_Jr
*/
/**
** @file   get_next_line.h
** @author Bender_Jr <bender[at]der-pc>
** @date   Fri May 26 10:47:19 2017
**
** @brief  gnl fonction struct and defines
**
*/
#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# include <unistd.h>

/**
** size of read () syscall readseze param
*/
#ifndef READ_SIZE
# define READ_SIZE (10)
# endif /* !READ_SIZE */


/**
** @struct s_gnl
** @brief store various variable during line parsing
** @var s_gnl::buff
** read () syscall buffer
** @var s_gnl::rd
** read () syscall return value (read len)
** @var s_gnl::j
** first increment
** @var s_gnl::i
** second increment
*/
/**
** @typedef t_gnl
** @brief mandatory typdef on s_gnl struct
*/
typedef struct	s_gnl {
  char		buff[READ_SIZE + 1];
  ssize_t	rd;
  int		j;
  int		i;
}		t_gnl;

/**
** @brief get next line take and open file descriptor, call read
** and
** @return buffered 'line' (as defined by '\n' char)
*/
char	*get_next_line(const int fd);

#endif /* !GET_NEXT_LINE_H_ */
