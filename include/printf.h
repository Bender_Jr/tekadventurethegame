/*
** printf.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:49:03 2017 Bender_Jr
** Last update Fri May 26 23:15:50 2017 Bender_Jr
*/

/**
** @file   printf.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:50:22 2017
**
** @brief standard io glibc printf re-code
** @note  better knowin' where when something failed
**
*/
#ifndef PRINTF_H_
# define PRINTF_H_

/**
** @def variadic arg macro could have been re-coded
**      too but hey !
*/
# include <stdarg.h>
# include "base.h"

#ifndef FLAG
# define FLAG  "dcsi"
# endif /* !FLAG */

int	flag_match(const char c);
int	flg_d(va_list list, int fd);
int	flg_i(va_list list, int fd);
int	flg_c(va_list list, int fd);
int	flg_s(va_list list, int fd);

int	p_printf(int fd, const char *format, ...);
int	p_putnbr(long nb, int fd);
void	flag_funct(int (*ptr[4])(va_list list, int fd));

#endif /* !PRINTF_H_ */
