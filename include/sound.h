/*
** sound.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:44:21 2017 Bender_Jr
** Last update Sat May 27 19:02:44 2017 Bender_Jr
*/

/**
** @file   sound.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 14:44:32 2017
**
** @brief  sound control objects and fction def
**
**
*/
#ifndef SOUND_H_
# define SOUND_H_

#include <SFML/Audio.h>

/**
** @struct s_music
** @brief store music handling data's
** @var s_music::nbsong
** song number in song dir
** @var s_music::curtrack
** current playing track index
** @var s_music::songname
** 2d char *array of music titles
** @var s_music::songz
** pointer on sfMusic elements
*/
/**
** @typedef t_music
** @brief mandatory typedef on s_music struct
*/
typedef struct s_music {
  int		nbsong;
  int		curtrack;
  char		**songname;
  sfMusic	**songz;
}		t_music;

/**
** @struct s_sound
** @brief store sound handling data's
** @var s_sound::nbsong
** song number in song dir
** @var s_sound::cursong
** current playing song index
** @var s_sound::songname
** 2d char *array of music titles
** @var s_sound::songz
** pointer on sfSoundBuffer elements
*/
/**
** @typedef t_sound
** @brief mandatory typedef on s_sound struct
*/
typedef struct s_sound {
  int		nbsong;
  int		cursong;
  char		**songname;
  sfSoundBuffer	**songz;
}		t_sound;

/**
** @struct s_loader
** @brief store song loader functions variables
** @var s_loader::namelist
** the open direcrory namelist as filled by scandir
** @var s_loader::path_save
** because scandir need a chdir so
** path_save in order to get back to previous directory
** @var s_loader::nb
** eleements number returned by scandir
*/
/**
** @typedef t_loader
** @brief mandatory typedef on s_loader struct
*/
typedef struct	s_loader {
  struct dirent **namelist;
  char		*path_save;
  int	        nb;
}		t_loader;

/**
** @brief fill the t_music struct
** @arg music directory dirname
** @return the filled t_music struct (see declarations)
*/
t_music		*get_music(const char *dirname);

/**
** @brief fill the t_music struct too but in this one
** use scandir to get filenames
** @arg music directory dirname
** @return the filled t_music struct (see declarations)
*/
t_music		*loadplaylist(const char *dirname);

/**
** @brief free the various fields of t_music struct
** @arg a free(able) pointer on t_music struct
** @return 0
*/
int		clean_zik(t_music *ptr);

/**
** @brief pitch track (self explanatory)
** @arg a t_music structure pointer
** @return 0
*/
int		pitch_track(void *obj);

/**
** @brief play_pause (self explanatory)
** @arg a t_music structure pointer
** @return 0
*/
int		play_pause(void *obj);

/**
** @brief get next track (self explanatory)
** @arg a t_music structure pointer
** @return 0
*/
int		next_track(void *obj);

/**
** @brief get previous track (self explanatory)
** @arg a t_music structure pointer
** @return 0
*/
int		prev_track(void *obj);

/**
** @brief fill the t_sound struct
** @arg music directory dirname
** @return the filled t_music struct (see declarations)
*/
t_sound		*get_sound(const char *dirname);

/**
** @brief fill the t_sound struct too but in this one
** use scandir to get filenames
** @arg sound directory dirname
** @return the filled t_sound struct (see declarations)
*/
t_sound		*loadsoundbank(const char *dirname);

/**
** @brief free the various fields of t_sound struct
** @arg a free(able) pointer on t_sound struct
** @return 0
*/
int		clean_sound(t_sound *ptr);

/**
** @brief fonction de génie , free deux struct en meme temps !
** @arg free(able) pointers on t_sound and t_music structs
** @return 0
*/
int		free_bufferz(t_sound *ptr, t_music *list);

#endif /* SOUND_H_ */
