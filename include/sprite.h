/*
** sprite.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat May 27 09:25:17 2017 Bender_Jr
** Last update Fri Jun  2 09:57:41 2017 Bender_Jr
*/

/**
** @file   sprite.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 09:25:49 2017
**
** @brief  sprite loading and drawings, element's definitions
**
*/
#ifndef SPRITE_H_
# define SPRITE_H_

# include <SFML/Graphics.h>

/**
** @struct s_sprite
** @brief  sprite drawing elements
** @var    s_sprite::sprite_name
** @brief  filename of the sprite
** @var    s_sprite::size_img
** @brief  Int array, define img x, y size
** @var    s_sprite::position
** @brief  Float array, define position on scene
** @var    s_sprite::offset
** @brief  Float array, define img offset
** @var    s_sprite::texture
** @brief  loading texture from image
** @var    s_sprite::img_sprite
** @brief  final sprite result
*/
/**
** @typedef t_sprite
** @brief   mandatory typedef on s_sprite struct
*/
typedef struct	s_sprite {
  char		*sprite_name;
  int		index;
  sfVector2i	size_img;
  sfVector2f	position, offset;
  sfTexture	*texture;
  sfSprite	*img_sprite;
}		t_sprite;

/**
** @brief as its name suggest, take
** @arg ptr
** @return a filled sprite tab on sucess , NULL on error
*/
t_sprite	*conf_to_sprite(t_conf *ptr);

/**
** @brief take sprite struct and draw sprite element on windows
** @return NULL
*/
void		*draw_sprite(sfRenderWindow *window, t_sprite *element);

/**
** @brief to get text sprite seting up
*/
sfText		*get_story(const char *message, t_conf *ptr);

#endif /* !SPRITE_H_ */
