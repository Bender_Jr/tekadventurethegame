/*
** strtowordtab.h for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:45:11 2017 Bender_Jr
** Last update Fri May 26 23:43:37 2017 Bender_Jr
*/

/**
** @file   strtowordtab.h
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 23:21:28 2017
**
** @brief strto_wordtab funct declaration
**
**
*/
#ifndef STRTOWORDTAB_H_
# define STRTOWORDTAB_H_

/**
** @struct t_count
** @brief store tokenizer data's
** @var t_count::linesize
** line size
** @var t_count::args
** token nbr
** @var t_count::line
** line offset
** @var t_count::i
** first incrementer
** @var t_count::j
** second incrementer
*/
/**
** @typedef s_count
** @brief mandatory typedef on t_conf struct
** @bug oui je me suis planté dans l'ordre des declarations
*/
typedef struct 	t_count {
  int		linesize;
  int		args;
  int		line;
  int		i;
  int		j;
}		s_count;

/**
** @brief tokenize in string based on delim
** @return a 2d array containing the tokens
*/
char	**strto_wordtab(char *str, const char *delim);

#endif /* !STRTOWORDTAB_H_ */
