/*
** base.c for Tekandventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:32:32 2017 Bender_Jr
** Last update Thu May 25 08:33:17 2017 Bender_Jr
*/

# include <unistd.h>
# include <stdlib.h>
# include "base.h"

int		pprint(const char *str, int fd)
{
  ssize_t	wr;

  wr = 0;
  if ((wr = write(fd, str, len(str))) == -1)
    return (84);
  return (0);
}

char		**freetab(char **ptr)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (ptr[i])
    i++;
  while (j < i)
    {
      free(ptr[j]);
      ptr[j] = NULL;
      j++;
    }
  free(ptr);
  return (NULL);
}

char	*clean_free(char *ptr)
{
  if (ptr)
    {
      free(ptr);
      ptr = NULL;
    }
  return (NULL);
}

int	my_atoi(const char *str)
{
  int	nb;

  nb = 0;
  while (*str)
    {
      if (*str >= '0' && *str <= '9')
	{
	  nb *= 10;
	  nb += *str - 48;
	}
      else
	return (nb);
      str++;
    }
  return (nb);
}
