/*
** base2.c for Tekandventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:32:03 2017 Bender_Jr
** Last update Thu May 25 08:32:07 2017 Bender_Jr
*/

# include <stdlib.h>
# include "base.h"

char		*epurstr(char *str, int c)
{
  size_t	i;
  size_t	j;
  char		*tmp;
  char		*clean;

  i = 0;
  j = 0;
  tmp = str;
  if ((clean = malloc(len(str) + 1)) == NULL ||
      !str || len(str) < 1)
    return (free(clean), tmp);
  while (str && (str[i] == ' ' || str[i] == '\t'))
    str++;
  while (str && str[i] != '\0')
    {
      while (!(str[i] > 32 && str[i] < 127)
	     && str[i] != '\0')
	i++;
      while (str[i] > 32 && str[i] < 127)
	clean[j++] = str[i++];
      clean[j++] = c;
    }
  free(tmp);
  return (clean[j - 1] = '\0', clean);
}

int	my_stringisnum(const char *str)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (str[i])
    {
      if ((str[i] >= '0') && (str[i] <= '9'))
	j = j + 1;
      i++;
    }
  if (j == i)
    return (j);
  else
    return (0);
}

char	*my_strcatvs(char *dest, const char *src)
{
  int	i;
  int	j;

  i = 0;
  j = len(dest);
  while (src[i])
    {
      dest[j + i] = src[i];
      i++;
    }
  dest[j + i] = '\0';
  return (dest);
}
