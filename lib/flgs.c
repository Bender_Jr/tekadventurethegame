/*
** flgs.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:34:10 2017 Bender_Jr
** Last update Thu May 25 08:34:11 2017 Bender_Jr
*/

/*
** for write
*/
# include <unistd.h>
# include "base.h"
# include "printf.h"

int	flg_d(va_list list, int fd)
{
  int	nb;

  nb = va_arg(list, int);
  p_putnbr(nb, fd);
  return (nb);
}

int	flg_i(va_list list, int fd)
{
  int	nb;

  nb = va_arg(list, int);
  p_putnbr(nb, fd);
  return (0);
}

int	flg_c(va_list list, int fd)
{
  int	a;

  a = va_arg(list, int);
  if (write(fd, &a, 1) == -1)
    return (-1);
  return (0);
}

int		flg_s(va_list list, int fd)
{
  const	char	*s;

  s = va_arg(list, char *);
  if (s == NULL)
    pprint("(null)", fd);
  pprint(s, fd);
  return (0);
}
