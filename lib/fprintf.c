/*
** fprintf.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:34:27 2017 Bender_Jr
** Last update Thu May 25 08:34:29 2017 Bender_Jr
*/

# include "base.h"

void		init_ptr(fpr *tab)
{
  tab[0] = f_str;
  tab[1] = f_long;
}

int		flag_run(char c, fpr *tab, FILE *stream, va_list list)
{
  char		flag[2];
  int		i;

  flag[0] = 's';
  flag[1] = 'd';
  i = 0;
  while (flag[i])
    {
      if (c == flag[i])
	return (tab[i](stream, list));
      i++;
    }
  return (0);
}

int		xfprintf(FILE *stream, const char *format, ...)
{
  va_list	list;
  fpr		ptr[2];
  int		i;

  init_ptr(ptr);
  i = 0;
  va_start(list, format);
  while (format[i])
    {
      if (format[i] == '%')
	{
	  flag_run(format[i + 1], ptr, stream, list);
	  i += 1;
	}
      else
	fwrite(&format[i], 1, 1, stream);
      i++;
    }
  va_end(list);
  return (0);
}
