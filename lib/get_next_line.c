/*
** get_next_line.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:34:48 2017 Bender_Jr
** Last update Thu May 25 08:35:19 2017 Bender_Jr
*/

# include <stdlib.h>
# include <unistd.h>
# include "base.h"
# include "get_next_line.h"

static char	*malloc_i_cat(char *dest, char src[],
			      int count, int *pos)
{
  char		*buffer;
  int		k;
  int		i;
  int		j;

  k = 0;
  i = -1;
  j = -1;
  if (dest != NULL)
    while (dest[k] != 0)
      k++;
  if ((buffer = malloc(k + count + 1)) == NULL)
    return (NULL);
  while ((i = i + 1) < k)
    buffer[i] = dest[i];
  while ((j = j + 1) < count)
    buffer[i + j] = src[*pos + j];
  buffer[i + j] = 0;
  if (dest != NULL)
    free(dest);
  *pos = *pos + count + 1;
  return (buffer);
}

char			*get_next_line(const int fd)
{
  char			*line;
  static t_gnl		args;

  line = NULL;
  args.i = 0;
  while (1)
    {
      if (args.j >= args.rd)
	{
	  args.j = 0;
	  if ((args.rd = read(fd, args.buff, READ_SIZE)) <= 0 ||
	      xstrchr(args.buff, 4))
	    return (line);
	  args.i = 0;
	}
      if (args.buff[args.j + args.i] == '\n' || args.buff[args.j + args.i] == 4)
	return (malloc_i_cat(line, args.buff, args.i, &(args).j));
      line = (args.j + args.i == args.rd - 1) ?
	malloc_i_cat(line, args.buff, args.i + 1, &(args).j) : line;
      args.i += 1;
    }
  free(line);
  return (NULL);
}
