/*
** printf.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:36:14 2017 Bender_Jr
** Last update Thu May 25 08:36:16 2017 Bender_Jr
*/

# include <unistd.h>
# include <limits.h>
# include <stdarg.h>

# include "printf.h"

int		p_printf(int fd, const char *format, ...)
{
  va_list	list;
  int		i;
  int		run;
  int		(*ptr[4])(va_list list, int fd);

  i = 0;
  run = 0;
  va_start(list, format);
  flag_funct(ptr);
  if (!(format))
    return (-1);
  while ((format[i]))
    {
      if (format[i] == '%' && (flag_match(format[i + 1]) != -1))
	{
	  run = flag_match(ptr[flag_match(format[i + 1])](list, fd));
	  i++;
	}
      else if (write(fd, &format[i], 1) == -1)
	return (-1);
      i++;
    }
  va_end(list);
  return (run);
}
