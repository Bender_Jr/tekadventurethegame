/*
** strchr.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:37:38 2017 Bender_Jr
** Last update Thu May 25 08:37:40 2017 Bender_Jr
*/

# include "base.h"

char		*xstrchr(const char *s, int c)
{
  size_t	i;
  char		*pos;

  i = 0;
  while (s[i])
    {
      pos = (char *)&s[i];
      if (c == *pos)
	return (pos);
      i++;
    }
  return (NULL);
}

char		*xstrrchr(const char *s, int c)
{
  size_t	i;
  char		*pos;

  i = len(s);
  while (i)
    {
      pos = (char *)&s[i];
      if (c == *pos)
	return (pos);
      i--;
    }
  return (NULL);
}
