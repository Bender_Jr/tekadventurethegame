/*
** stringbis.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu May 25 08:37:52 2017 Bender_Jr
** Last update Thu May 25 13:19:33 2017 Bender_Jr
*/

# include "base.h"
# include "printf.h"

size_t		tab_len(char  **bfr)
{
  size_t	i;

  i = 0;
  if (bfr[i])
    {
      while (bfr[i])
	i++;
    }
  return (i);
}

#ifdef STD_C_99
void		*my_memmove(void *dest, const void *src, size_t n)
{
  char		*char_src;
  char		*char_dest;
  char		tmp[n + 1];
  size_t	i;

  i = 0;
  char_src = (char *)src;
  char_dest = (char *)dest;
  xmemset(tmp, '\0', sizeof(tmp));
  while (i != n)
    {
      tmp[i] = char_src[i];
      i++;
    }
  i = 0;
  while (i != n)
    {
      char_dest[i] = tmp[i];
      i++;
    }
  return (char_dest);
}
#endif

char		*my_strncpy(char *dest, const char *src, size_t n)
{
  size_t	i;

  i = 0;
  while (i < n && src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  while (i < n)
    {
      dest[i] = '\0';
      i++;
    }
  return (dest);
}

void		*xmemset(void *s, int c, size_t n)
{
  size_t	i;

  i = 0;
  while (i != n)
    {
      ((unsigned char *)s)[i] = c;
      i++;
    }
  return (s);
}

char		*removespace(char *str)
{
  size_t	i;

  i = 0;
  while (str[i] == ' ')
    ++str;
  while (str[i])
    {
      if (str[i] == ' ')
	{
	  str[i] = str[i + 1];
	  return (str);
	}
      ++i;
    }
  return (str);
}
