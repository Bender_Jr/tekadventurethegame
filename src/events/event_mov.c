/*
** event_mov.c for tekadventure in /home/kasbi_y/Graphique/tekadventure/src/events
**
** Made by Yacine Kasbi
** Login   <kasbi_y@epitech.net>
**
** Started on  Sat May 27 18:24:25 2017 Yacine Kasbi
** Last update Fri Jun  2 10:33:15 2017 Bender_Jr
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "game.h"
#include "base.h"

int		move_up(void *ptr)
{
  t_sprite	*cast;

  cast = (t_sprite *)ptr;
  if (cast[cast->index].img_sprite)
    {
#ifdef DEBUG
      fprintf(stdout, "[mv.up] %.2f :: %.2f\n",
	      cast[cast->index].position.x, cast[cast->index].position.y);
#endif
      cast[cast->index].position =
	sfSprite_getPosition(cast[cast->index].img_sprite);
      cast[cast->index].position.y = cast[cast->index].position.y - 8;
      sfSprite_setPosition(cast[cast->index].img_sprite,
			   cast[cast->index].position);
      return (0);
    }
  return (1);
}

int		move_down(void *ptr)
{
  t_sprite	*cast;

  cast = (t_sprite *)ptr;
  if (cast[cast->index].img_sprite)
    {
#ifdef DEBUG
      fprintf(stdout, "[mv.down] %.2f :: %.2f\n",
	      cast[cast->index].position.x, cast[cast->index].position.y);
#endif
      cast[cast->index].position =
	sfSprite_getPosition(cast[cast->index].img_sprite);
      cast[cast->index].position.y = cast[cast->index].position.y + 8;
      sfSprite_setPosition(cast[cast->index].img_sprite,
			   cast[cast->index].position);
      return (0);
    }
  return (1);
}

int		move_right(void *ptr)
{
  t_sprite	*cast;

  cast = (t_sprite *)ptr;
  if (cast[cast->index].img_sprite)
    {
#ifdef DEBUG
      fprintf(stdout, "[mv.right] %.2f :: %.2f\n",
	      cast[cast->index].position.x, cast[cast->index].position.y);
#endif
      cast[cast->index].position =
	sfSprite_getPosition(cast[cast->index].img_sprite);
      cast[cast->index].position.x = cast[cast->index].position.x + 8;
      sfSprite_setPosition(cast[cast->index].img_sprite,
			   cast[cast->index].position);
      return (0);
    }
  return (1);
}

int		move_left(void *ptr)
{
  t_sprite	*cast;

  cast = (t_sprite *)ptr;
  if (cast[cast->index].img_sprite)
    {
#ifdef DEBUG
      fprintf(stdout, "[mv.left] %.2f :: %.2f\n",
	      cast[cast->index].position.x, cast[cast->index].position.y);
#endif
      cast[cast->index].position =
	sfSprite_getPosition(cast[cast->index].img_sprite);
      cast[cast->index].position.x = cast[cast->index].position.x - 8;
      sfSprite_setPosition(cast[cast->index].img_sprite,
			   cast[cast->index].position);
      return (0);
    }
  return (1);
}
