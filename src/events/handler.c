/*
** handler.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sun May 28 11:29:33 2017 Bender_Jr
** Last update Fri Jun  2 12:59:48 2017 Bender_Jr
*/

#include "game.h"
#include "base.h"

/**
** @brief construct sound handler key and operations
**	  fonctions
** @return a t_sound_event ptr tab filled
*/
static void	*get_sound_opts(t_sound_event *ptr)
{
  ptr[0].key = 0;
  ptr[0].act = NULL;
  ptr[1].key = sfKeyP;
  ptr[1].act = play_pause;
  ptr[2].key = sfKeyN;
  ptr[2].act = next_track;
  ptr[3].key = sfKeyX;
  ptr[3].act = prev_track;
  ptr[4].key = -1;
  ptr[4].act = NULL;
  return (ptr);
}

/**
** @brief construct move handler key and operations
**	  fonctions
** @return a t_move_event ptr tab filled
*/
static void	*get_move_opts(t_move_event *ptr)
{
  ptr[0].key = 0;
  ptr[0].act = NULL;
  ptr[1].key = sfKeyUp;
  ptr[1].act = move_up;
  ptr[2].key = sfKeyDown;
  ptr[2].act = move_down;
  ptr[3].key = sfKeyRight;
  ptr[3].act = move_right;
  ptr[4].key = sfKeyLeft;
  ptr[4].act = move_left;
  ptr[5].key = -1;
  ptr[5].act = NULL;
  return (ptr);
}

/**
** @brief find if event is a move event if true
** @return event index else 0
*/
static int	is_move_event(t_move_event *tab, sfKeyCode key, int *index)
{
  int		i;

  i = *index;
  while (tab[i].key != -1)
    {
      if (key == tab[i].key)
	return (*index = i);
      ++i;
    }
  return (*index = 0);
}

/**
** @brief find if event is a sound event if true
** @return event index else 0
*/
static int	is_sound_event(t_sound_event *tab, sfKeyCode key, int *index)
{
  int		i;

  i = *index;
  while (tab[i].key != -1)
    {
      if (key == tab[i].key)
        return (*index = i);
      ++i;
    }
  return (*index = 0);
}

void		event_gestion(sfEvent evt, sfRenderWindow *window, void *ptr)
{
  int		i;
  t_game	*cast;
  t_event	obj;

  i = 0;
  cast = (t_game *)ptr;
  get_sound_opts(obj.hdlr);
  get_move_opts(obj.moov);
  if (evt.type == sfEvtKeyPressed)
    {
      i = is_sound_event(obj.hdlr, evt.key.code, &i) ? obj.hdlr[i].act(cast->music):
	is_move_event(obj.moov, evt.key.code, &i) ? obj.moov[i].act(cast->sprite):
	0;
      if (evt.key.code == sfKeyEscape)
	sfRenderWindow_close(window);
    }
  else if (evt.type == sfEvtClosed)
    sfRenderWindow_close(window);
  else if (evt.type == sfEvtMouseButtonPressed &&
	   evt.mouseButton.button == sfMouseLeft)
    sfRenderWindow_drawSprite(window, cast->sprite[G1].img_sprite, NULL);
}
