/*
** destructor.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Wed May 31 00:47:47 2017 Bender_Jr
** Last update Thu Jun  1 11:31:38 2017 Bender_Jr
*/

#include <stdlib.h>
#include "game.h"

/**
** @file   destructor.c
** @author Bender_Jr <bender@bender-pc>
** @date   Wed May 31 00:49:11 2017
**
** @brief game loop ressources destructor
*/
void		*clean_ressources(t_game *ptr)
{
  int		i;

  i = 0;
  while (ptr->conf[i].f_path)
    {
      free(ptr->conf[i].m_path);
      free(ptr->conf[i].font_path);
      free(ptr->sprite[i].sprite_name);
      sfTexture_destroy(ptr->sprite[i].texture);
      sfSprite_destroy(ptr->sprite[i].img_sprite);
      ++i;
    }
  clean_zik(ptr->music);
  return (ptr);
}
