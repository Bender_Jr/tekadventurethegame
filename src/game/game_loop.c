/*
** game_loop.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:07:46 2017 Bender_Jr
** Last update Fri Jun  2 13:18:26 2017 Bender_Jr
*/

/**
** @file   game_loop.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:39:05 2017
**
** @brief  preliminary game loop (also a test for event handler)
**
*/
#include <stdlib.h>
#include <unistd.h>
#include <SFML/Graphics.h>
#include <SFML/Window.h>
#include "game.h"
#include "base.h"
#include "get_next_line.h"

static void		*set_obj(t_sprite *sprites)
{
  static sfVector2f	obj_pos = {0,0};

  obj_pos.x = sprites[P1].position.x + 8;
  obj_pos.y = sprites[P1].position.y + 32;
  sfSprite_setPosition(sprites[G1].img_sprite, obj_pos);
  return (NULL);
}

static void		*xdraw_scene(t_game *ptr, sfRenderWindow *window, sfView *cur_view)
{
  int			i;

  i = -1;
  while (ptr->conf[++i].f_path)
    {
      if (i != CMAP)
	sfRenderWindow_drawSprite(window, ptr->sprite[i].img_sprite, NULL);
    }
  sfRenderWindow_drawText(window, ptr->text[0], NULL);
  sfView_setCenter(cur_view, ptr->sprite[P1].position);
  sfRenderWindow_setView(window, cur_view);
  return (NULL);
}

int			game_loop(t_game *ptr)
{
  sfRenderWindow	*window;
  sfView		*cur_view;
  sfEvent		event;
  static int		opts = (sfResize | sfClose);
  static sfVideoMode	mode = {WINDOW_HEIGHT, WINDOW_WIDTH, 32};

  cur_view = sfView_create();
  sfView_zoom(cur_view, 0.3);
  if ((window = sfRenderWindow_create(mode, NAME, opts, NULL)) == NULL)
    return (p_printf(2, "\nsfRenderwind_create failed\n"), 84);
  while (sfRenderWindow_isOpen(window))
    {
      ptr->sprite->index = P1;
      sfRenderWindow_clear(window, sfBlack);
      while (sfRenderWindow_pollEvent(window, &event))
      	event_gestion(event, window, ptr);
      ptr->sprite[G1].img_sprite ? set_obj(ptr->sprite) : ptr;
      xdraw_scene(ptr, window, cur_view);
      sfRenderWindow_display(window);
    }
  clean_ressources(ptr);
  return (0);
}
