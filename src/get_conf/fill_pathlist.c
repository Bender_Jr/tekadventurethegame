/*
** fill_pathlist.c for TEkadventure in /home/bender/Repo/tekadventurethegame
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Thu Jun  1 11:04:44 2017 Bender_Jr
** Last update Thu Jun  1 11:12:09 2017 Bender_Jr
*/

/**
** @file   fill_pathlist.c
** @author Bender_Jr <bender@bender-pc>
** @date   Thu Jun  1 11:04:54 2017
**
** @brief  store fill files fields on conf struct
**
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "conf.h"
#include "base.h"

int		fill_mpath(char *bfr, t_conf *ptr)
{
  if ((open(bfr, O_CLOEXEC)) == -1)
    return (p_printf(2, "\n./Tekadventure: %s no such file or directory\n",
		     bfr), -1);
  ptr->m_path = bfr;
  return (0);
}

int		fill_fpath(char *bfr, t_conf *ptr)
{
  if ((open(bfr, O_CLOEXEC)) == -1)
    return (p_printf(2, "\n./Tekadventure: %s no such file or directory\n",
		     bfr), -1);
  ptr->f_path = bfr;
  return (0);
}

int		fill_font_path(char *bfr, t_conf *ptr)
{
  if ((open(bfr, O_CLOEXEC)) == -1)
    return (p_printf(2, "\n./Tekadventure: %s no such file or directory\n",
		     bfr), -1);
  ptr->font_path = bfr;
  return (0);
}
