/*
** fill_struct.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 08:50:29 2017 Bender_Jr
** Last update Thu Jun  1 11:11:10 2017 Bender_Jr
*/

/**
** @file   fill_struct.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:12:38 2017
**
** @brief all the t_voc function pointer fill ;
** Take buffer, check data and fill sprite_conf struct
**
**
*/
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "conf.h"
#include "base.h"

int		fill_size(char *bfr, t_conf *ptr)
{
  char		**tmp;

  tmp = strto_wordtab(bfr, ":");
  if (tab_len(tmp) != 2 ||
      (ptr->size_img[0] = my_atoi(tmp[0])) == -1 ||
      (ptr->size_img[1] = my_atoi(tmp[1])) == -1)
    {
      freetab(tmp);
      p_printf(2, "fill_size failed\n");
      return (-1);
    }
  return (freetab(tmp), 0);
}

int		fill_pos(char *bfr, t_conf *ptr)
{
  char		**tmp;

  tmp = strto_wordtab(bfr, ":");
  if (tab_len(tmp) != 2 ||
      (ptr->position[0] = atof((tmp[0]))) == -1 ||
      (ptr->position[1] = atof(tmp[1])) == -1)
    {
      freetab(tmp);
      p_printf(2, "./Tekadventure :fill_pos failed\n");
      return (-1);
    }
  return (freetab(tmp), 0);
}

int		fill_scale(char *bfr, t_conf *ptr)
{
  char		**tmp;

  tmp = strto_wordtab(bfr, ":");
  if (tab_len(tmp) != 2 ||
      (ptr->scale[0] = atof((tmp[0]))) == -1 ||
      (ptr->scale[1] = atof(tmp[1])) == -1)
    {
      freetab(tmp);
      p_printf(2, "fill_scale failed\n");
      return (-1);
    }
  return (freetab(tmp), 0);
}
