/*
** get_conf.c for TEKADEVNTURE in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Wed May 24 13:27:53 2017 Bender_Jr
** Last update Thu Jun  1 10:45:49 2017 Bender_Jr
*/

/**
** @file   get_conf.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 09:49:34 2017
**
** @brief  parseur / lexer operations conf file
**         and fill corresponding fields in
**	   t_sprite_conf struct
**
**
*/
#include <stdlib.h>
#include <stdio.h>
#include <SFML/Graphics.h>
#include "base.h"
#include "get_next_line.h"
#include "conf.h"

/**
** @brief Compare first bits of line buffer
** to check for a known lexer keyword
** @return position in lex_tab or
** -1 in case of unknow / missing keyword
*/
static int	is_ind(char *line, t_voc *lex_tab, int *pos)
{
  int		i;

  i = 0;
  while (lex_tab[i].index >= -1)
    {
      if ((strn_cmp(lex_tab[i].ind, line, len(lex_tab[i].ind))))
	return (*pos = i);
      ++i;
    }
  *pos = 0;
  return (p_printf(2, "is_ind failed on [%s]\n", line), -1);
}

/**
** @brief Extract data from end of start indicator
** to the first '<' symbol (1st char of END_IND array)
** @return clean data buffer
**
*/
static void	*get_char(char *bfr, char *cpy, size_t i)
{
  size_t	j;

  j = 0;
  while (bfr[i] != *END_IND)
    {
      if (bfr[i] >= '.' && bfr[i] <= 'z')
	{
	  cpy[j] = bfr[i];
	  if ((j = j + 1) == MAX_KWD)
	    return (p_printf(2, "get_char failed on [%s]\n",
			     bfr), NULL);
	}
      ++i;
    }
  cpy[j] = '\0';
  return (cpy);
}

void		*get_data(char *bfr, t_voc pos, t_conf *fill)
{
  static int	new_elem = 0;
  char		cpy[MAX_KWD];
  char		*clean;

  clean = NULL;
  xmemset(cpy, '\0', MAX_KWD);
  new_elem += pos.index == -1 ? 1 : 0;
  if (pos.index != -1)
    {
      if ((get_char(bfr, cpy, len(pos.ind))) == NULL)
	return (NULL);
      clean = my_strdup(cpy);
      if (pos.fill_xxx &&
	  pos.fill_xxx(clean, &fill[new_elem]) == -1)
	return (NULL);
      return (fill);
    }
  return (fill);
}

/**
** @brief check for the end keyword (</>)
** following a start one (like <f_name>)
** @return boolean , data or NULL
*/
static void	*check_synt(char *bfr, void *data)
{
  if ((strn_cmp(bfr + (len(bfr) - len(END_IND)), END_IND, len(END_IND))))
    return (data);
  else
    return (p_printf(2, "checksynth failed on [%s]\n", bfr), NULL);
}

t_conf		*get_conf(const int c_filefd, int element_nbr)
{
  char		*line;
  int		pos;
  t_conf	*conf;
  t_voc		lex_tab[7];

  pos = 0;
  init_lex(lex_tab);
  conf = new_elem(element_nbr);
  while ((line = epurstr(get_next_line(c_filefd), ' ')))
    {
      if ((line[0] != CMT_IND) && len(line) >= 3 &&
	  ((is_ind(line, lex_tab, &pos) == -1) ||
	   check_synt(line, lex_tab[pos].ind) == NULL))
	return (clean_buff(conf, line), NULL);
      else if (line[0] != CMT_IND && len(line) >= 3)
	get_data(line, lex_tab[pos], conf);
      free(line);
    }
  close(c_filefd);
  return (conf);
}
