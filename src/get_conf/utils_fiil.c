/*
** utils_fiil.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 08:53:46 2017 Bender_Jr
** Last update Fri Jun  2 11:51:14 2017 Bender_Jr
*/

/**
** @file   utils_fiil.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Fri May 26 10:15:53 2017
**
** @brief  mainly the lexer struct constructor here
** and utils funct link print, malloc , free etc
**
**
*/

#include <stdlib.h>
#include "conf.h"
#ifdef DEBUG
# include "sprite.h"
#endif
#include "base.h"

#ifdef DEBUG
void		print_test(t_conf *fill, void *ptr)
{
  int		i;
  t_sprite	*cast;

  i = 0;
  cast = (t_sprite *)ptr;
  while (fill[i].f_path)
    {
      p_printf(1, ":sound bank [%s]\n", fill[i].m_path);
      p_printf(1, ":fonts dir [%s]\n", fill[i].font_path);
      p_printf(1, ":f_name [%s]\n", fill[i].f_path);
      p_printf(1, ":size [%d][%d]\n", fill[i].size_img[0], fill[i].size_img[1]);
      printf(":pos [%.2f][%.2f]\n", fill[i].position[0], fill[i].position[1]);
      printf(":scale [%.2f][%.2f]\n", fill[i].scale[0], fill[i].scale[1]);
      p_printf(1, "\n\tSPRITE conf :: \n Name : [%s]", cast[i].sprite_name);
      p_printf(1, "\n\tsize [%d][%d]\n", cast[i].size_img.x,
	       cast[i].size_img.y);
      printf("\n\tpos [%.2f][%.2f]\n", cast[i].position.x, cast[i].position.y);
      printf("\n\tscale [%.2f][%.2f]\n\n", cast[i].offset.x, cast[i].offset.y);
      ++i;
    }
  p_printf(1, "\nNumber of assets : %d\n\n", i);
}
#endif

void		*new_elem(int size)
{
  t_conf	*new;

  new = NULL;
  if ((new = malloc(sizeof(*new) * size + 1)) == NULL)
    return (p_printf(2, "new_elem failed (enomem ?)\n"), NULL);
  xmemset(new, '\0', sizeof(*new));
  new->f_path = NULL;
  new->m_path = NULL;
  new->font_path = NULL;
  return (new);
}

void		clean_buff(void *c1, void *c2)
{
  p_printf(2, "get_conf bam !\n");
  free(c1);
  free(c2);
}

/**
** @brief pseudo constructor helper
** for t_voc tab
** @return a filled t_voc struct field
*/
static void	*get_case(t_voc *ptr, int index, char *name, parseur fction)
{
  ptr->index = index;
  my_strcpy(ptr->ind, name);
  ptr->fill_xxx = fction;
  return (name);
}

void	*init_lex(t_voc *lex_tab)
{
  get_case(&lex_tab[0], 0, NAME_IND, fill_fpath);
  get_case(&lex_tab[1], 1, SIZE_IND, fill_size);
  get_case(&lex_tab[2], 2, POS_IND, fill_pos);
  get_case(&lex_tab[3], 3, SCALE_IND, fill_scale);
  get_case(&lex_tab[4], 4, SOUND_IND, fill_mpath);
  get_case(&lex_tab[5], 5, FONT_IND, fill_font_path);
  get_case(&lex_tab[6], -1, END_IND, NULL);
  return (lex_tab);
}
