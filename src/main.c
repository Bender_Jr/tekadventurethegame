/*
** main.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 15:44:53 2017 Bender_Jr
** Last update Fri Jun  2 13:02:20 2017 Bender_Jr
*/

/**
** @file   main.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 13:39:26 2017
**
** @brief The tekadventure entry point
**
*/
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "game.h"
#include "base.h"

/**
** @brief exec entry point check for $DISPLAY
**        in env and other function failure
** @arg   for the moment nothing
** @return 0 in sucess 84 otherwise
*/
int			main(void)
{
  int			conf_fd;
  t_game		game;

  if ((getenv("DISPLAY")))
    {
      if ((conf_fd = open(CONF, O_RDONLY)) == -1)
	return (p_printf(2, "Err no conf file\n"), 84);
      if ((game.conf = get_conf(conf_fd, 6)) == NULL ||
	  (game.music = get_music(game.conf->m_path)) == NULL ||
	  (game.text[0] = get_story("Tek-ad-Ventr's", game.conf)) == NULL ||
	  (game.sprite = conf_to_sprite(game.conf)) == NULL)
	return (84);
#ifdef DEBUG
      print_test(game.conf, game.sprite);
#endif
      game_loop(&game);
      return (0);
    }
  return (p_printf(2, "%s\n", ENV_ERR), 84);
}
