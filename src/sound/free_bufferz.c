/*
** free_bufferz.c for TEKAdventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:48:14 2017 Bender_Jr
** Last update Mon May 29 18:11:21 2017 Bender_Jr
*/

/**
** @file   free_bufferz.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:40:35 2017
**
** @brief  useless funct to free two buffer in one funct
**
**
*/
#include "sound.h"

int	free_bufferz(t_sound *ptr, t_music *list)
{
  clean_zik(list);
  clean_sound(ptr);
  return (0);
}
