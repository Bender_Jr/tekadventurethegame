/*
** music.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:48:40 2017 Bender_Jr
** Last update Tue May 30 13:38:56 2017 Bender_Jr
*/

/**
** @file   music.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:42:25 2017
**
** @brief  including music loading from dir fonctions
**
*/
#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "sound.h"
#include "base.h"

static int	filter(const struct dirent *entry)
{
  size_t	fl;
  size_t	extl;

  fl = strlen(entry->d_name);
  extl = strlen(".ogg");
  return (strn_cmp(entry->d_name + fl - extl, ".ogg", extl));
}

static void	*catch_err(const char *dirname, struct dirent ***namelist,
			  t_music *list)
{
  int		rt;

  rt = 0;
  p_printf(1, "[%s]\n", dirname);
  if ((rt = scandir(dirname, namelist, filter, NULL)) == -1 ||
      (list = malloc(sizeof(*list))) == NULL ||
      (list->songz = malloc(sizeof(*list->songz) * rt + 1)) == NULL ||
      (list->songname = malloc(sizeof(*list->songname) * rt + 1)) == NULL ||
      chdir(dirname) == -1)
    return (perror("Err loadplaylist:"), NULL);
  list->nbsong = rt;
  return (list);
}

static void	*clean_objs(t_loader *objs)
{
  free(objs->namelist);
  chdir(objs->path_save);
  free(objs->path_save);
  return (NULL);
}

static void	*init_loader(t_loader *addr)
{
  addr->path_save = NULL;
  addr->path_save = getcwd(addr->path_save, 256);
  addr->nb = 0;
  return (addr);
}

t_music			*loadplaylist(const char *dirname)
{
  static t_loader	objs;
  t_music		*list;


  list = NULL;
  init_loader(&objs);
  if ((list = catch_err(dirname, &(objs).namelist, list)) == NULL)
    return (NULL);
  objs.nb = list->nbsong;
  while (objs.nb--)
    {
#ifdef DEBUG
      printf("\nLoad : Track N°%d \n\tTitle [%s]\n", objs.nb, objs.namelist[objs.nb]->d_name);
#endif
      list->songname[objs.nb] = my_strdup(objs.namelist[objs.nb]->d_name);
      if ((list->songz[objs.nb] = sfMusic_createFromFile(objs.namelist[objs.nb]->d_name))
	  == NULL)
	return (p_printf(2, "sfMusic failed\n"), NULL);
      free(objs.namelist[objs.nb]);
    }
  clean_objs(&objs);
  return (list);
}
