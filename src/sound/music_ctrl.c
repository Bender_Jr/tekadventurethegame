/*
** music_ctrl.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:50:10 2017 Bender_Jr
** Last update Mon May 29 18:11:57 2017 Bender_Jr
*/

/**
** @file   music_ctrl.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:44:28 2017
**
** @brief  all music control fonction (as activate by
** the event handler protocol
*/
#ifdef DEBUG
# include <stdio.h>
# include "base.h"
#endif

#include "sound.h"

int		play_pause(void *obj)
{
 static  int	i = 1;
 t_music *ptr;

 i++;
 ptr = (t_music *)obj;
 if (!(i % 2))
   {
     sfMusic_play(ptr->songz[ptr->curtrack]);
#ifdef DEBUG
     printf("Play track [%s]\n",  ptr->songname[ptr->curtrack]);
#endif
     return (0);
   }
 else
   {
     sfMusic_pause(ptr->songz[ptr->curtrack]);
#ifdef DEBUG
     printf("Paused track [%s]\n", ptr->songname[ptr->curtrack]);
#endif
     return (0);
   }
 return (-1);
}

int		next_track(void *obj)
{
  t_music	*ptr;

  ptr = (t_music *)obj;
  if (ptr->curtrack != ptr->nbsong - 1)
    ptr->curtrack += 1;
#ifdef DEBUG
  printf("Next track [%s]\n",  ptr->songname[ptr->curtrack]);
#endif
  if (ptr->curtrack == ptr->nbsong)
    ptr->curtrack = 0;
  return (0);
}

int		prev_track(void *obj)
{
  t_music	*ptr;

  ptr = (t_music *)obj;
  if (ptr->curtrack > 0)
    ptr->curtrack -= 1;
#ifdef DEBUG
  printf("Prev track [%s]\n",  ptr->songname[ptr->curtrack]);
#endif
  if (ptr->curtrack == -1)
    ptr->curtrack = ptr->nbsong - 1;
  return (0);
}

int		pitch_track(void *obj)
{
  t_music	*ptr;
  float		i;

  i = 1.5;
  ptr = (t_music *)obj;
  sfMusic_setPitch(ptr->songz[ptr->curtrack], i + 0.5);
#ifdef DEBUG
  printf("Pitch curtrack to [%f]\n", i);
#endif
  return (0);
}
