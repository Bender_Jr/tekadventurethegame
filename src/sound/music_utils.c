/*
** music_utils.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat May 27 12:05:17 2017 Bender_Jr
** Last update Tue May 30 20:59:33 2017 Bender_Jr
*/

/**
** @file   music_utils.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 12:11:53 2017
**
** @brief  main get music funct and clean zik
**
*/
# include <stdlib.h>
# include "sound.h"
# include "base.h"

int	clean_zik(t_music *ptr)
{
  int	i;

  i = ptr->nbsong;
  while (--i != -1)
    {
      sfMusic_destroy(ptr->songz[i]);
      free(ptr->songname[i]);
    }
  free(ptr->songname);
  free(ptr);
  return (0);
}

t_music		*get_music(const char *dirname)
{
  t_music	*list;

  if ((list = loadplaylist(dirname)) == NULL)
    return (p_printf(2, "Get_music failed\n"), NULL);
#ifdef DEBUG
  printf("\nThere is \033[1m[%d]\033[0m music in playlist\n", list->nbsong);
#endif
  list->curtrack = 0;
  return (list);
}
