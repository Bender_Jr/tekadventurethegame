/*
** sound.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Fri May 26 14:50:50 2017 Bender_Jr
** Last update Sat May 27 15:00:40 2017 Bender_Jr
*/

/**
** @file   sound.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:46:29 2017
**
** @brief all sound management loading and ctrl
*/
#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "sound.h"
#include "base.h"

int	clean_sound(t_sound *ptr)
{
  int	i;

  i = ptr->nbsong;
  while (i-- != 0)
    {
      sfSoundBuffer_destroy(ptr->songz[i]);
      free(ptr->songname[i]);
    }
  free(ptr->songname);
  free(ptr);
  return (0);
}

t_sound		*get_sound(const char *dirname)
{
  t_sound	*list;

  if ((list = loadsoundbank(dirname)) == NULL)
    return (NULL);
#ifdef DEBUG
  printf("\nThere is \033[1m[%d]\033[0m sound in soundbank\n", list->nbsong);
#endif
  return (list);
}

/**
** @brief filter file by extension
** @return pos number on sucess 0 otherwise
*/
int		get_ext(const struct dirent *entry)
{
  size_t	fl;
  size_t	extl;

  fl = strlen(entry->d_name);
  extl = strlen(".ogg");
  return (strn_cmp(entry->d_name + fl - extl, ".ogg", extl));
}

t_sound		*loadsoundbank(const char *dirname)
{
  int		rt;
  struct dirent **namelist;
  t_sound	*list;

  if ((rt = scandir(dirname, &namelist, &get_ext, NULL)) == -1 ||
      (list = malloc(sizeof(*list))) == NULL ||
      (list->songz = malloc(sizeof(*list->songz) * rt + 1)) == NULL ||
      (list->songname = malloc(sizeof(*list->songname) * rt + 1)) == NULL ||
      chdir(dirname) == -1)
    return (perror("loadsoundbank:"), NULL);
  list->nbsong = rt;
  while (rt --)
    {
#ifdef DEBUG
      printf("\nLoad : song N°%d \n\tTitle [%s]\n", rt, namelist[rt]->d_name);
#endif
      list->songname[rt] = my_strdup(namelist[rt]->d_name);
      if ((list->songz[rt] = sfSoundBuffer_createFromFile(namelist[rt]->d_name))
	  == NULL)
	return (NULL);
      free(namelist[rt]);
    }
  free(namelist);
  return (list);
}
