/*
** sprite.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat May 27 09:20:56 2017 Bender_Jr
** Last update Fri Jun  2 11:52:26 2017 Bender_Jr
*/

/**
** @file   conf_to_sprite.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 10:37:51 2017
**
** @brief  contains the main load sprite from conf fctions
**
*/
#include <stdlib.h>
#include "game.h"
#include "base.h"

/**
** @brief set the sprite original coordinates
** @return vector2i and 2f set up
**
*/
static void	*set_vectors(t_sprite *new, t_conf ptr)
{
  sfVector2i	a;
  sfVector2f	b;
  sfVector2f	c;


  a.x = ptr.size_img[0];
  a.y = ptr.size_img[1];
  new->size_img = a;
  b.x = ptr.position[0];
  b.y = ptr.position[1];
  new->position = b;
  c.x = ptr.scale[0];
  c.y = ptr.scale[1];
  new->offset = c;
  return (new);
}

/**
** @brief malloc and memset a new t_sprite object
**
** @param element_nbr
**
** @return a malloc'd t_sprite of sprite struct size *elem
*/
static t_sprite		*get_elems(t_conf *ptr)
{
  int			element_nbr;
  t_sprite		*new;

  element_nbr = 0;
  while (ptr[element_nbr].f_path)
    ++element_nbr;
  if ((new = malloc(sizeof(*new) * element_nbr + 1)) == NULL)
    return (NULL);
  return (new);
}

/**
** @brief set sprite from loaded image
** @param *new
** @param i
** @return a t_sprite
** @bug for the moment character sprite
**      is splited based on hard coded value
**      (this will change soon)
*/
static void		*set_sprite(t_sprite *new)
{
  sfIntRect		perso;

  sfTexture_setSmooth(new->texture, sfTrue);
  sfSprite_setTexture(new->img_sprite, new->texture, sfTrue);
  sfSprite_setPosition(new->img_sprite, new->position);
  if (new->index == P1)
    {
      perso.left = perso.top = 0;
      perso.width = 85;
      perso.height = 145;
      sfSprite_setTextureRect(new->img_sprite, perso);
    }
  return (new);
}

t_sprite		*conf_to_sprite(t_conf *ptr)
{
  int			i;
  t_sprite		*new;

  i = 0;
  if ((new = get_elems(ptr)) == NULL)
    return (NULL);
  while (ptr[i].f_path)
    {
      new[i].sprite_name = ptr[i].f_path;
      new[i].index = i;
      if ((new[i].img_sprite = sfSprite_create()) == NULL ||
	  (new[i].texture =
	   sfTexture_createFromFile(new[i].sprite_name, NULL)) == NULL)
	return (NULL);
      set_vectors(&new[i], ptr[i]);
      set_sprite(&new[i]);
      ++i;
    }
  return (new);
}
