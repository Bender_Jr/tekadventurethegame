/*
** draw_sprite.c for TEkadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Sat May 27 14:15:08 2017 Bender_Jr
** Last update Tue May 30 10:36:58 2017 Bender_Jr
*/

/**
** @file   draw_sprite.c
** @author Bender_Jr <bender[at]bender-pc>
** @date   Sat May 27 14:20:59 2017
**
** @brief  take sprite from struct and render it on window
** @bug    --there is also an ugly fixed value for now-- ??
** ====>>>>>>>>> FUCKING SOLVED <<<<<<<<<<<<<<<===
** note : see element->index ? (current pointeur to print ctrl, all
** function are connected)
*/
#include <unistd.h>
#include "game.h"
#include "base.h"

void		*draw_sprite(sfRenderWindow *window, t_sprite *element)
{
  sfRenderWindow_drawSprite(window, element->img_sprite, NULL);
  return (NULL);
}
