/*
** get_character_sprite.c for TEKadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Mon May 29 20:31:54 2017 Bender_Jr
** Last update Mon May 29 20:44:23 2017 Bender_Jr
*/

/**
** @file   get_character_sprite.c
** @author Peter_Xu <peter.xu[at]epitech.eu>
** @date   Mon May 29 20:33:39 2017
**
** @brief get_character sprite self (splite a sprite in sprite tab)
**
*/
#include <stdlib.h>
#include <SFML/Graphics.h>
#include "game.h"

/**
** @brief get_rect_sprite : Renvoie le sprite "découpé"
**
** @arg :
**		- sprite : Le sprite de référence
**		- image : L'image de référence
**		- nbImage : Vector contenant le nombre
**			d'image par ligne et le nombre
**			de ligne
**		- count : Une variable permettant de calculer l'emplacement de
**			la découpe
**
** @return  :
**		- Le pointeur du sprite "découpé"
*/

sfSprite	*get_rect_sprite(sfSprite *sprite,
				 sfImage *image,
				 sfVector2i size_img,
				 int count)
{
  sfIntRect	rect;
  sfSprite	*sprite_split;

  if ((sprite_split = sfSprite_copy(sprite)) == NULL)
    return (NULL);
  rect.width = sfImage_getSize(image).x / size_img.x;
  rect.height = sfImage_getSize(image).y / size_img.y;
  rect.left = ((count % size_img.x) * rect.width);
  rect.top = ((count / size_img.x) * rect.height);
  if (count % size_img.x == 0)
    rect.left = 0;
  sfSprite_setTextureRect(sprite_split, rect);
  return (sprite_split);
}

/**
** @brief create_tab_sprite : Créé un tableau de pointeur de sprite
**
** @arg nbImage :
** @brief Vector contenant le nombre d'image par ligne et le nombre
** de ligne
**
** @return Le tableau de pointeur de sprite créer ou NULL
** en cas d'erreur
*/
sfSprite	**create_tab_sprite(sfVector2i size_img)
{
  int		i;
  sfSprite	**sprite_tab;

  i = 0;
  sprite_tab = malloc(sizeof(sfSprite *) * (size_img.x * size_img.y));
  while (i < (size_img.x * size_img.y))
    {
      if (!(sprite_tab[i] = sfSprite_create()))
	return (NULL);
      i++;
    }
  return (sprite_tab);
}

/**
**	@biref get_split_sprite : Créer un tableau de sprite
**
**      @arg :
**		- sprite : Le sprite de référence
**		- image : L'image de référence
**		- size_img : Vector contenant le nombre
**			d'image par ligne et le nombre
**			de ligne
**
**	@return :
**		- Retourne le tableau de sprite complétés
**			ou NULL en cas d'erreur
**
*/
sfSprite	**get_split_sprite(sfSprite *sprite,
				   sfImage *image,
				   sfVector2i size_img)
{
  int		i;
  sfSprite	**sprite_tab;

  i = 0;
  if (!(sprite_tab = create_tab_sprite(size_img)))
    return (NULL);
  while (i < (size_img.x * size_img.y))
    {
      sprite_tab[i] = get_rect_sprite(sprite, image, size_img, i);
      i++;
    }
  return (sprite_tab);
}
