/*
** sprite_mov.c for tekadventure in /home/kasbi_y/Graphique/tekadventure/src/sprite
**
** Made by Yacine Kasbi
** Login   <kasbi_y@epitech.net>
**
** Started on  Sat May 27 18:49:56 2017 Yacine Kasbi
** Last update Mon May 29 18:12:55 2017 Bender_Jr
*/

#include <SFML/Graphics/RenderWindow.h>
#include <SFML/Graphics.h>
#include <SFML/Graphics/Sprite.h>
#include <SFML/Graphics/Texture.h>
#include <stdlib.h>
#include <math.h>

/**
** @brief fonction pour faire bouger des sprite ,
** n'importe quelle sprite peut être manipulé,
** les valeurs de pos.x dans les if peuvent et doivent être modifiés pour
** être conformes avec la taille de la fenetre utilisée
*/
sfVector2f	move_it(sfSprite *sprite, sfVector2f pos,
			sfRenderWindow *window, float speed)
{
  sfVector2f	speed_pos;

  speed_pos.x = speed;
  speed_pos.y = 0;
  sfSprite_move(sprite, speed_pos);
  pos = sfSprite_getPosition(sprite);
  if (speed < 0)
    {
      if (pos.x < -1080)
	pos.x = 1080;
    }
  else
    if (pos.x > 1200)
      pos.x = -1200;
  sfSprite_setPosition(sprite, pos);
  sfRenderWindow_drawSprite(window, sprite, NULL);
  return (pos);
}

/**
** @brief Mouvement en arc-de-cercle parfait ,
** la vitesse de rotation peut étre fixer dans les parametre
** la largeur de l'arc-de-cercle peut etre modifier : valeur de base
** 400 .
*/
sfVector2f	arc_de_cercle(sfSprite *sprite, sfRenderWindow *window, float speed)
{
  static float	angle = 0;
  sfVector2f	pos;
  sfVector2u	win;

  if (angle > 2 * M_PI)
    angle = 0;
  win = sfRenderWindow_getSize(window);
  pos.x = cos(angle) * 400 + win.x / 2;
  pos.y = sin(angle) * 400 + win.y / 2;
  angle = angle + speed;
  sfSprite_move(sprite, pos);
  sfSprite_setPosition(sprite, pos);
  sfRenderWindow_drawSprite(window, sprite, NULL);
  pos = sfSprite_getPosition(sprite);
  return (pos);
}
