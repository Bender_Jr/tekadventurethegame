/*
** text_sprite.c for Tekadventure in /home/bender/Repo/tekadventure
**
** Made by Bender_Jr
** Login   <@epitech.eu>
**
** Started on  Wed May 31 00:32:58 2017 Bender_Jr
** Last update Fri Jun  2 09:58:21 2017 Bender_Jr
*/

/**
** @file   text_sprite.c
** @author Bender_Jr <bender@bender-pc>
** @date   Wed May 31 00:36:47 2017
**
** @brief include an utility to get text with fonts
**
*/
#include "game.h"

sfText			*get_story(const char *message, t_conf *ptr)
{
  sfVector2f		text_pos;
  sfFont		*police;
  sfText		*story;

  text_pos.x = 3444;
  text_pos.y = 188;
  story = sfText_create();
  police = sfFont_createFromFile(ptr->font_path);
  sfText_setString(story, message);
  sfText_setFont(story, police);
  sfText_setCharacterSize(story, 30);
  sfText_setPosition(story, text_pos);
  return (story);
}
